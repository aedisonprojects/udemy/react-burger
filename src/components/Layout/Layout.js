import React, { Component, Fragment } from 'react';
import classes from './Layout.module.css';

import Toolbar from 'components/Toolbar/Toolbar';
import Sidebar from 'components/Sidebar/Sidebar';

class Layout extends Component {

  state = {
    sidebar: false
  };

  hideSidebar = () => {
    this.setState({ sidebar: false });
  };

  toggleSidebar = () => {
    this.setState((prevState) => {
      return { sidebar: !prevState.sidebar };
    })
  };

  render() {
    return (
      <Fragment>
        <Sidebar show={this.state.sidebar} hide={() => this.hideSidebar()} />
        <Toolbar onSidebarToggleClick={() => this.toggleSidebar()} />
        <main className={classes.Content}>
          {this.props.children}
        </main>
      </Fragment>
    );
  }
}

export default Layout;