import React, { Component } from 'react';
import classes from './Toolbar.module.css';

import Logo from 'components/Logo/Logo';
import Navigation from 'components/Navigation/Navigation';
import SidebarToggle from 'components/SidebarToggle/SidebarToggle';

class Toolbar extends Component {
  render() {
    return (
      <header className={classes.Toolbar}>
        <SidebarToggle onClick={this.props.onSidebarToggleClick} />
        <Logo context="Toolbar" />
        <Navigation context="Toolbar" />
      </header>
    );
  }
}

export default Toolbar;