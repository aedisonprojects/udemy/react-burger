import React, { Component, Fragment } from 'react';
import classes from './OrderSummary.module.css';
import Button from 'components/Button/Button';

class OrderSummary extends Component {

  shouldComponentUpdate = (nextProps, nextState) => {
    return true;
  };

  getIngredientsList = () => {
    const ingredients = this.props.ingredients;
    const keys = Object.keys(ingredients);
    const list = keys.map(key => {
      return (
        <li key={key}>
          <span className={classes.TitleCase}>{key}</span>: {ingredients[key]}
        </li>
      );
    });

    return list;
  };


  render() {
    return (
      <Fragment>
        <h3>Your Order</h3>
        <p>A delicious burger with the following ingredients:</p>
        <ul>
          {this.getIngredientsList()}
        </ul>
        <p><strong>Total Price: {this.props.price}</strong></p>
        <p>Continue to Checkout?</p>
        <Button type='Danger' click={this.props.dismiss}>CANCEL</Button>
        <Button type='Success' click={this.props.continue}>CONTINUE</Button>
      </Fragment>
    );
  }
}

export default OrderSummary;