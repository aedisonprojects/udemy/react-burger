import React, { Component } from 'react';
import classes from './Button.module.css';

class Button extends Component {
  render() {
    const displayClasses = [classes.Button, classes[this.props.type]].join(' ');

    return (
      <button
        className={displayClasses}
        onClick={this.props.click}
      >{this.props.children}</button>
    );
  }
}

export default Button;