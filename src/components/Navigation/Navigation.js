import React, { Component } from 'react';
import classes from './Navigation.module.css';

import NavigationItem from 'components/NavigationItem/NavigationItem';

class Navigation extends Component {
  render() {

    const displayClasses = [classes.Navigation, classes[this.props.context]].join(' ');

    return (
      <nav>
        <ul className={displayClasses}>
          <NavigationItem link="/" active>Burder Builder</NavigationItem>
          <NavigationItem link="/">Checkout</NavigationItem>
        </ul>
      </nav>
    );
  }
}

export default Navigation;