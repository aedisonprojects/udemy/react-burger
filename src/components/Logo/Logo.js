import React, { Component } from 'react';

import burgerLogo from 'assets/images/burger-logo.png';
import classes from './Logo.module.css';

class Logo extends Component {
  render() {

    const displayClasses = [classes.Logo, classes[this.props.context]].join(' ');

    return (
      <div className={displayClasses} >
        <img src={burgerLogo} alt="Burger Builder Logo" />
      </div>
    );
  }
}

export default Logo;