import React, { Component } from 'react';
import classes from './SidebarToggle.module.css';

class SidebarToggle extends Component {
  render() {
    return (
      <div
        className={classes.SidebarToggle}
        onClick={this.props.onClick}>
        <div></div>
        <div></div>
        <div></div>
      </div>
    );
  }
}

export default SidebarToggle;