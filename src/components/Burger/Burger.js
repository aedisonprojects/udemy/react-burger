import React, { Component } from 'react';

import classes from './Burger.module.css';
import BurgerIngredient from '../BurgerIngredient/BurgerIngredient';

class Burger extends Component {


  transformIngredients() {
    let ingredients = this.props.ingredients;
    let transformed = [];

    for (let key in ingredients) {
      for (let i = 0; i < ingredients[key]; i++) {
        transformed.push(<BurgerIngredient key={key + i} type={key} />);
      }
    }

    return transformed;
  }

  render() {

    const transformedIngredients = this.transformIngredients();
    const ingredients = (transformedIngredients.length > 0) ? transformedIngredients : 'Please start adding ingredients!';

    return (
      <div className={classes.Burger}>
        <BurgerIngredient type='BreadTop' />
        {ingredients}
        <BurgerIngredient type='BreadBottom' />
      </div>
    );
  }
}

export default Burger;