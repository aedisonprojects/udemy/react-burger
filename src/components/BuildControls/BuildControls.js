import React, { Component } from 'react';

import classes from './BuildControls.module.css';
import BuildControl from 'components/BuildControls/BuildControl/BuildControl';

const controls = [
  { label: 'Lettuce', type: 'lettuce' },
  { label: 'Bacon', type: 'bacon' },
  { label: 'Cheese', type: 'cheese' },
  { label: 'Meat', type: 'meat' },
];

class BuildControls extends Component {

  renderControls(controls) {
    let rendered = controls.map(control => {
      return (
        <BuildControl
          key={control.label}
          label={control.label}
          add={() => this.props.add(control.type)}
          remove={() => this.props.remove(control.type)}
        />
      );
    });

    return rendered;
  }

  render() {
    return (
      <div className={classes.BuildControls}>
        <p>Current Price: <strong>{this.props.price.toFixed(2)}</strong></p>
        {this.renderControls(controls)}
        <button
          className={classes.OrderButton}
          onClick={this.props.showSummary}
          disabled={!this.props.order}>ORDER NOW</button>
      </div>
    );
  }
}

export default BuildControls;