import React, { Component, Fragment } from 'react';
import classes from './Modal.module.css';
import Backdrop from 'components/Backdrop/Backdrop';

class Modal extends Component {

  shouldComponentUpdate = (nextProps, nextState) => {
    return nextProps.show !== this.props.show;
  };

  render() {

    const displayClass = (this.props.show) ? classes.ModalShow : classes.ModalHide;

    return (
      <Fragment>
        <Backdrop show={this.props.show} click={this.props.click} />
        <div className={[classes.Modal, displayClass].join(' ')}>
          {this.props.children}
        </div>
      </Fragment>
    );
  }
}

export default Modal;