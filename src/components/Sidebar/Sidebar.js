import React, { Component, Fragment } from 'react';
import classes from './Sidebar.module.css';
import Logo from 'components/Logo/Logo';
import Navigation from 'components/Navigation/Navigation';
import Backdrop from 'components/Backdrop/Backdrop';


class Sidebar extends Component {
  render() {

    const displayClass = (this.props.show) ? classes.Open : classes.Close;
    const classNames = [classes.Sidebar, displayClass].join(' ');

    return (
      <Fragment>
        <Backdrop show={this.props.show} click={this.props.hide} />
        <div className={classNames}>
          <Logo context="Sidebar" />
          <Navigation />
        </div>
      </Fragment>
    );
  }
}

export default Sidebar;