import React, { Component, Fragment } from 'react';
import Burger from 'components/Burger/Burger';
import BuildControls from 'components/BuildControls/BuildControls';
import Modal from 'components/Modal/Modal';
import OrderSummary from '../../components/OrderSummary/OrderSummary';


const INGREDIENT_PRICES = {
  lettuce: 0.3,
  cheese: 0.2,
  meat: 1.2,
  bacon: 0.7
};

const BASE_PRICE = 2;

class BurgerBuilder extends Component {

  state = {
    ingredients: {
      lettuce: 0,
      bacon: 0,
      cheese: 0,
      meat: 0
    },
    price: BASE_PRICE,
    order: false,
    summary: false
  };

  checkout = () => {
    alert('Continuing to checkout...');
  };

  showSummary = () => {
    this.setState({ summary: true });
  };

  hideSummary = () => {
    this.setState({ summary: false });
  };

  canOrder = (ingredients) => {
    let ingredientAdded = Object.values(ingredients).some(amount => amount > 0);
    return ingredientAdded;
  };

  removeIngredient = (type) => {
    let minimum = 0;
    const ingredients = { ...this.state.ingredients };

    if (ingredients[type] === minimum) {
      return;
    }

    ingredients[type]--;
    const price = this.state.price - INGREDIENT_PRICES[type];
    const order = this.canOrder(ingredients);
    this.setState({ ingredients, price, order });
  };

  addIngredient = (type) => {
    let maximum = 2;
    const ingredients = { ...this.state.ingredients };

    if (ingredients[type] === maximum) {
      return;
    }

    ingredients[type]++;
    const price = this.state.price + INGREDIENT_PRICES[type];
    const order = this.canOrder(ingredients);
    this.setState({ ingredients, price, order });
  };

  render() {
    return (
      <Fragment>
        <Modal show={this.state.summary} click={() => this.hideSummary()}>
          <OrderSummary
            price={this.state.price.toFixed(2)}
            ingredients={this.state.ingredients}
            dismiss={() => this.hideSummary()}
            continue={() => this.checkout()}
          />
        </Modal>
        <Burger ingredients={this.state.ingredients} />
        <BuildControls
          add={this.addIngredient}
          remove={this.removeIngredient}
          price={this.state.price}
          order={this.state.order}
          showSummary={() => this.showSummary()} />
      </Fragment>
    );
  }
}

export default BurgerBuilder;